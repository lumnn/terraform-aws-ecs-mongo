resource "aws_efs_file_system" "backups" {
  creation_token = "${var.project_name}"

  tags = {
    Name = "${var.project_name}"
  }
}

resource "aws_efs_mount_target" "efs_mount" {
  for_each = data.aws_subnet_ids.selected.ids

  file_system_id  = aws_efs_file_system.backups.id
  subnet_id       = each.value
  security_groups = [aws_security_group.backups.id]
}

resource "aws_security_group" "backups" {
  name = "${var.project_name}-backups-efs"
  vpc_id = data.aws_vpc.selected.id

  ingress {
    from_port       = 2049
    to_port         = 2049
    protocol        = "tcp"
    security_groups = var.backup_efs_allow_sg
  }
}
