#!/usr/bin/env bash

# shellcheck disable=SC1091
. /opt/bitnami/scripts/libmongodb_orig.sh

########################
# Get if primary node is initialized
# Globals:
#   MONGODB_*
# Arguments:
#   $1 - node
# Returns:
#   None
#########################
mongodb_is_primary_node_initiated() {
    local node="${1:?node is required}"
    local result
    result=$(
        mongodb_execute "$MONGODB_ROOT_USER" "$MONGODB_ROOT_PASSWORD" "admin" "127.0.0.1" "$MONGODB_PORT_NUMBER" <<EOF
rs.initiate({"_id":"$MONGODB_REPLICA_SET_NAME", "members":[{"_id":0,"host":"$node:${MONGODB_ADVERTISED_PORT_NUMBER:-MONGODB_PORT_NUMBER}","priority":5}]})
EOF
    )

    # Code 23 is considered OK
    # It indicates that the node is already initialized
    if grep -q "\"code\" : 23" <<<"$result"; then
        warn "Node already initialized."
        return 0
    fi

    if ! grep -q "\"ok\" : 1" <<<"$result"; then
        warn "Problem initiating replica set
            request: rs.initiate({\"_id\":\"$MONGODB_REPLICA_SET_NAME\", \"members\":[{\"_id\":0,\"host\":\"$node:${MONGODB_ADVERTISED_PORT_NUMBER:-MONGODB_PORT_NUMBER}\",\"priority\":5}]})
            response: $result"
        return 1
    fi
}

########################
# Get if secondary node is pending
# Globals:
#   MONGODB_*
# Arguments:
#   $1 - node
# Returns:
#   Boolean
#########################
mongodb_is_secondary_node_pending() {
    local node="${1:?node is required}"
    local result

    mongodb_set_dwc

    result=$(
        mongodb_execute "$MONGODB_INITIAL_PRIMARY_ROOT_USER" "$MONGODB_INITIAL_PRIMARY_ROOT_PASSWORD" "admin" "$MONGODB_INITIAL_PRIMARY_HOST" "$MONGODB_INITIAL_PRIMARY_PORT_NUMBER" <<EOF
rs.add({host: '$node:${MONGODB_ADVERTISED_PORT_NUMBER:-MONGODB_PORT_NUMBER}'})
EOF
    )
    debug "$result"
    # Error code 103 is considered OK.
    # It indicates a possiblely desynced configuration,
    # which will become resynced when the secondary joins the replicaset.
    if grep -q "\"code\" : 103" <<<"$result"; then
        warn "The ReplicaSet configuration is not aligned with primary node's configuration. Starting secondary node so it syncs with ReplicaSet..."
        return 0
    fi
    grep -q "\"ok\" : 1" <<<"$result"
}

########################
# Get if arbiter node is pending
# Globals:
#   MONGODB_*
# Arguments:
#   $1 - node
# Returns:
#   Boolean
#########################
mongodb_is_arbiter_node_pending() {
    local node="${1:?node is required}"
    local result

    mongodb_set_dwc

    result=$(
        mongodb_execute "$MONGODB_INITIAL_PRIMARY_ROOT_USER" "$MONGODB_INITIAL_PRIMARY_ROOT_PASSWORD" "admin" "$MONGODB_INITIAL_PRIMARY_HOST" "$MONGODB_INITIAL_PRIMARY_PORT_NUMBER" <<EOF
rs.addArb('$node:${MONGODB_ADVERTISED_PORT_NUMBER:-MONGODB_PORT_NUMBER}')
EOF
    )
    grep -q "\"ok\" : 1" <<<"$result"
}

