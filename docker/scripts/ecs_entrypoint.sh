#!/usr/bin/env bash

set -euo pipefail

MONGODB_ADVERTISED_HOSTNAME=$(curl --silent http://169.254.169.254/latest/meta-data/local-ipv4)
MONGODB_ADVERTISED_PORT_NUMBER=$(curl --silent "$ECS_CONTAINER_METADATA_URI_V4" | jq '.Ports | map(select(.ContainerPort = 27017))[0].HostPort')

export MONGODB_ADVERTISED_HOSTNAME
export MONGODB_ADVERTISED_PORT_NUMBER

echo export MONGODB_ADVERTISED_HOSTNAME - "$MONGODB_ADVERTISED_HOSTNAME"
echo export MONGODB_ADVERTISED_PORT_NUMBER - "$MONGODB_ADVERTISED_PORT_NUMBER"

echo CMD "$@"
exec /opt/bitnami/scripts/mongodb/entrypoint.sh "$@"
