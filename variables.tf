variable "project_name" {
  description = "A underscore separated name of deployed module. Will influence names of tasks, roles, etc."
  type = string
  default = "mongo"
}

variable "docker_image" {
  description = "An image to use for mongo container"
  type = string
  default = "bitnami/mongodb:3.6.23"
}

variable "vpc_name" {
  description = "Name of the VPC. This influences the mount targets for EFS backups"
  type = string
  default = "default"
}

variable "backup_efs_allow_sg" {
  description = "List of security groups that should be allowed to access EFS network drive (port 2049) with backups. "
  type = list(string)
  nullable = false
}
