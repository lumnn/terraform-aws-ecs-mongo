resource "aws_iam_role" "task_execution_role" {
  name = "${local.project_name_title}ECSTaskExecutionRole"

  assume_role_policy = jsonencode({
    Version = "2012-10-17"
    Statement = [
      {
        Action = "sts:AssumeRole"
        Effect = "Allow"
        Sid    = ""
        Principal = {
          Service = "ecs-tasks.amazonaws.com"
        }
      },
    ]
  })

  inline_policy {
    name = "${local.project_name_title}AllowPutLogEvents"
    policy = jsonencode({
      Version = "2012-10-17"
      Statement = [
        {
          Effect = "Allow"
          Action = [
            "logs:PutLogEvents",
            "logs:CreateLogStream"
          ]
          Resource = [
            aws_cloudwatch_log_group.task.arn,
            "${aws_cloudwatch_log_group.task.arn}:log-stream:*",
            aws_cloudwatch_log_group.task_backups.arn,
            "${aws_cloudwatch_log_group.task_backups.arn}:log-stream:*",
          ]
        }
      ]
    })
  }
}
