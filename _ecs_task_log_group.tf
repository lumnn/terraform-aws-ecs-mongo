resource "aws_cloudwatch_log_group" "task" {
  name = "/ecs/${var.project_name}"
  retention_in_days = 7
}

resource "aws_cloudwatch_log_group" "task_backups" {
  name = "/ecs/${var.project_name}-backups"
  retention_in_days = 7
}
