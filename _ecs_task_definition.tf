resource "aws_ecs_task_definition" "mongo" {
  family                   = var.project_name
  #memory                   = 2048
  requires_compatibilities = ["EC2"]
  execution_role_arn       = aws_iam_role.task_execution_role.arn

  container_definitions    = jsonencode([
    {
      name        = "restore_data"
      image       = "registry.gitlab.com/lumnn/docker-mongo-backup-helper/master:latest"
      essential   = false
      memory      = 128
      command     = ["restore_mongo_files.sh", "latest"]
      mountPoints = [
        {
          sourceVolume  = "backups"
          containerPath = "/backups"
        },
        {
          sourceVolume  = "mongo_storage"
          containerPath = "/bitnami/mongodb"
        }
      ]
      environment = [
        {
          name = "MONGO_DATA"
          value = "/bitnami/mongodb"
        }
      ]
      logConfiguration = {
        logDriver = "awslogs"
        options = {
          awslogs-group         = aws_cloudwatch_log_group.task_backups.name
          awslogs-region        = data.aws_region.current.name
          awslogs-stream-prefix = "ecs"
        }
      }
    },
    {
      name               = "mongo"
      image              = var.docker_image
      essential          = true
      memoryReservation  = 1024
      # healthCheck = {
      #   command     = ["CMD-SHELL", "mongo --quiet --eval 'db.runCommand(\"ping\").ok'"]
      #   startPeriod = 60
      #   interval    = 30
      #   timeout     = 10
      #   rerties     = 5
      # }
      environment = [
        {
          name = "MONGODB_REPLICA_SET_MODE"
          value = "primary"
        },
        {
          name = "MONGODB_REPLICA_SET_NAME"
          value = "rs0"
        },
        {
          name = "MONGODB_ROOT_PASSWORD"
          value = "examplePw"
        },
        {
          name = "MONGODB_REPLICA_SET_KEY"
          value = "test1234"
        }
      ]
      dependsOn = [{
        containerName = "restore_data"
        condition     = "SUCCESS"
      }]
      portMappings = [
        {
          containerPort = 27017
          hostPort      = 0
          protocol      = "tcp"
        }
      ],
      mountPoints = [
        {
          sourceVolume  = "mongo_storage"
          containerPath = "/bitnami/mongodb"
        }
      ]
      logConfiguration = {
        logDriver = "awslogs"
        options = {
          awslogs-group         = aws_cloudwatch_log_group.task.name
          awslogs-region        = data.aws_region.current.name
          awslogs-stream-prefix = "ecs"
        }
      }
    },
    {
      name        = "backup"
      image       = "registry.gitlab.com/lumnn/docker-mongo-backup-helper/master:latest"
      essential   = true
      memory      = 128
      command     = ["backup_mongo.sh", "--schedule", "0 3 * * *"]
      stopTimeout = 120
      dependsOn   = [{
        containerName = "restore_data"
        condition     = "SUCCESS"
      }]
      mountPoints = [
        {
          sourceVolume  = "backups"
          containerPath = "/backups"
        },
        {
          sourceVolume  = "mongo_storage"
          containerPath = "/bitnami/mongodb"
        }
      ]
      environment = [
        {
          name = "MONGO_DATA"
          value = "/bitnami/mongodb"
        }
      ]
      logConfiguration = {
        logDriver = "awslogs"
        options = {
          awslogs-group         = aws_cloudwatch_log_group.task_backups.name
          awslogs-region        = data.aws_region.current.name
          awslogs-stream-prefix = "ecs"
        }
      }
    }
  ])

  volume {
    name = "mongo_storage"

    docker_volume_configuration {
      scope         = "task"
    }
  }

  volume {
    name = "backups"

    efs_volume_configuration {
      file_system_id     = aws_efs_file_system.backups.id
      transit_encryption = "ENABLED"
    }
  }
}
