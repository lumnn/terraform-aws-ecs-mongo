terraform {
  required_providers {
    aws = {
      version = "~> 3.74"
      source  = "hashicorp/aws"
    }
  }
}

locals {
  project_name_title = replace(title(replace(var.project_name, "_", " "))," ","")
}

data "aws_region" "current" {}
data "aws_caller_identity" "current" {}

data "aws_vpc" "selected" {
  tags = {
    Name = var.vpc_name
  }
}

data "aws_subnet_ids" "selected" {
  vpc_id = data.aws_vpc.selected.id
}
